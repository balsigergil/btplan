<?php

namespace App\Http\Controllers;

use App\Holiday;
use App\Type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class ConfigController extends Controller
{
    public function index(){
    	$holidays = Holiday::orderBy('date')->get();
    	$years = [];
    	foreach ($holidays as $holiday){
    		if(!in_array($holiday->date->year, $years)){
    			array_push($years, $holiday->date->year);
		    }
	    }
	    sort($years);

	    $types = Type::all();

    	return view('config', ['holidays' => $holidays, 'years' => $years, 'types' => $types]);
    }
}
