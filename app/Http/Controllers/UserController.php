<?php

namespace App\Http\Controllers;

use App\Http\Resources\BirthResource;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$users = User::orderBy('firstname')->get();
        return view('users.index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
        	'firstname' => 'required|max:255',
        	'lastname' => 'required|max:255',
	        'muid' => 'required|unique:users|max:255',
	        'email' => 'required|email|unique:users|max:255',
	        'address' => 'max:255'
        ]);

        $user = new User();
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->muid = $request->muid;
        $user->email = $request->email;
        $user->address = $request->address;
        $user->birth = $request->birth;
        $user->phone = $request->phone;
        $user->save();

        return redirect()->route('users.index')->with('status', "L'utilisateur {$request->firstname} {$request->lastname} a été créé avec succès");
    }

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\User $user
	 *
	 * @return void
	 */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('users.edit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
	    $request->validate([
		    'firstname' => 'required|max:255',
		    'lastname' => 'required|max:255',
		    'muid' => [
		    	'required',
			    Rule::unique('users')->ignore($user->id),
			    'max:255'
		    ],
		    'email' => [
			    'required',
			    Rule::unique('users')->ignore($user->id),
			    'max:255'
		    ],
		    'address' => 'max:255'
	    ]);

	    $user->firstname = $request->firstname;
	    $user->lastname = $request->lastname;
	    $user->muid = $request->muid;
	    $user->email = $request->email;
	    $user->address = $request->address;
	    $user->birth = $request->birth;
	    $user->phone = $request->phone;
	    $user->save();

	    return redirect()->route('users.index')->with('status', "L'utilisateur {$request->firstname} {$request->lastname} a été modifié avec succès");
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\User $user
	 *
	 * @return \Illuminate\Http\Response
	 * @throws \Exception
	 */
    public function destroy(User $user)
    {
        $user->delete();
	    return redirect()->route('users.index')->with('status', "L'utilisateur {$user->firstname} {$user->lastname} a été supprimé avec succès");
    }

    public function getAllBirths(Request $request){
    	$users = User::whereNotNull('birth')->get();
    	$col = new Collection();
    	foreach ($users as $user){
    		for($i = date('Y'); $i < date('Y') + 3; $i++){
    			$newUser = $user->replicate();
			    $newUser->birth = Carbon::createFromDate($i, $user->birth->month, $user->birth->day);
			    $col->push($newUser);
		    }
		    for($i = date('Y')-1; $i >= date('Y') - 3; $i--){
			    $newUser = $user->replicate();
			    $newUser->birth = Carbon::createFromDate($i, $user->birth->month, $user->birth->day);
			    $col->push($newUser);
		    }
	    }
    	return BirthResource::collection($col);
    }
}
