<?php

namespace App\Http\Controllers;

use App\Holiday;
use Illuminate\Http\Request;

class HolidayController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('holidays.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	    $request->validate([
	    	'name' => 'required|max:255',
		    'date' => 'required'
	    ]);
    	$holiday = new Holiday();
    	$holiday->name = $request->name;
    	$holiday->date = $request->date;
        $holiday->save();
        return redirect()->route('config')->with('status', "{$request->name} ajouté avec succès");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Holiday  $holiday
     * @return \Illuminate\Http\Response
     */
    public function edit(Holiday $holiday)
    {
	    return view('holidays.edit')->with('holiday', $holiday);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Holiday  $holiday
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Holiday $holiday)
    {
	    $request->validate([
		    'name' => 'required|max:255',
		    'date' => 'required'
	    ]);
        $holiday->name = $request->name;
        $holiday->date = $request->date;
        $holiday->save();
        return redirect()->route('config')->with('status', "{$request->name} modifié avec succès");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Holiday  $holiday
     * @return \Illuminate\Http\Response
     */
    public function destroy(Holiday $holiday)
    {
	    $holiday->delete();
	    return redirect()->route('config');
    }
}
