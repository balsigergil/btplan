<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class BirthResource extends Resource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request
	 *
	 * @return array
	 */
	public function toArray( $request ) {

		$title = '<i class="fa fa-birthday-cake"></i> ' . $this->firstname . ' ' . $this->lastname . ' <i class="fa fa-birthday-cake"></i>';
		$bgc   = '#FF0000';

		if ( $this->birth ) {
			return [
				'title'           => $title,
				'start'           => $this->birth->format( 'Y-m-d' ),
				'end'             => $this->birth->format( 'Y-m-d' ),
				'backgroundColor' => $bgc,
				'borderColor'     => $bgc,
				'isBirth'         => true
			];
		} else {
			return [];
		}
	}
}
