$(document).ready(function () {
    let data = {
        labels: ["Gil Balsiger", "Christian Pittet", "Florent Zahnd", "Fabrice Galley"],

        datasets: [
            {
                values: [25, 40, 15, 25]
            }
        ]
    };

    let data2 = {
        labels: ["Gil Balsiger", "Christian Pittet", "Florent Zahnd", "Fabrice Galley"],

        datasets: [
            {
                values: [15, 25, 10, 5]
            }
        ]
    };

    let chart = new Chart({
        parent: "#chart",
        title: "Jours de piquet par personne en 2017",
        data: data,
        type: 'bar',
        height: 250,
        colors: ['violet'],
        format_tooltip_y: d => d + ' jours'
    });

    let chart2 = new Chart({
        parent: "#chart-2",
        title: "Jours de vacances par personne en 2017",
        data: data2,
        type: 'bar',
        height: 250,
        colors: ['orange'],
        format_tooltip_y: d => d + ' jours'
    });

    let chart3 = new Chart({
        parent: "#chart-3",
        title: "Jours de vacances par personne en 2017",
        data: data2,
        type: 'line',
        height: 250,
        colors: ['orange'],
        format_tooltip_y: d => d + ' jours'
    });

    let chart4 = new Chart({
        parent: "#chart-4",
        title: "Jours de vacances par personne en 2017",
        data: data2,
        type: 'pie',
        height: 250,
        colors: ['orange'],
        format_tooltip_y: d => d + ' jours'
    });
});