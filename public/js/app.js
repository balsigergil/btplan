$(document).ready(function () {
    var currentdDate;
    if ($('#dateUpdated').text() !== '') {
        currentdDate = $('#dateUpdated').text()
    }else{
        currentdDate = moment();
    }
    //console.log($('#dateUpdated').text());

    $('#calendar').fullCalendar({
        locale: 'fr',
        header: {
            right: 'today prev,next'
        },
        defaultDate: currentdDate,
        selectable: true,
        selectHelper: true,
        select: function (start, end) {
            $('#createLeaveModal').modal();
            $('#startDateCreate').datepicker('update', start.format('YYYY-M-D'));
            $('#endDateCreate').datepicker('update', end.subtract(1, 'days').format('YYYY-M-D'));
            $('#calendar').fullCalendar('unselect');
        },
        eventSources: [
            {
                url: '/leaves'
            }, {
                url: '/births'
            }
        ],
        eventRender: function (event, element) {
            element.find('.fc-title').html(event.title);
            if (event.isBirth)
                element.find('.fc-content').css('text-align', 'center');
        },
        eventClick: function (event) {
            if (event.isBirth) {

            } else {
                $('#updateLeaveModal').modal();
                $('#select_user_update').val(event.user_id).trigger('change');
                $('#select_type_update').val(event.type_id).trigger('change');
                $('#startDateUpdate').datepicker('update', event.start.format('YYYY-M-D'));
                $('#endDateUpdate').datepicker('update', event.end.subtract(1, 'days').format('YYYY-M-D'));
                $('#commentUpdate').val(event.comment);
                $('#deleteBtn').attr('href', '/leaves/' + event.id + '/delete');
                $('#updateForm').attr('action', '/leaves/' + event.id);
            }
        }
    });

    $("input#color").colpick({
        submit: 0,
        onBeforeShow: function () {
            $(this).colpickSetColor(this.value);
        },
        onChange: function (hsb, hex, rgb, el, bySetColor) {
            $(el).css('border-color', '#' + hex);
            if (!bySetColor) $(el).val('#' + hex);
        }
    }).keyup(function () {
        $(this).colpickSetColor(this.value);
    });

    $('.datepicker').datepicker({
        format: "yyyy-mm-dd",
        language: "fr"
    });

    $('.select2').select2({
        theme: "bootstrap",
        width: '100%'
    });

    $('.input-daterange').datepicker({
        format: "yyyy-mm-dd",
        language: "fr"
    });

});