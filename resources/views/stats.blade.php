@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Statistiques</h1>
        <hr>
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">Piquet</div>
                    <div class="card-body">
                        <div id="chart"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">Vacances</div>
                    <div class="card-body">
                        <div id="chart-2"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">Graphique 3</div>
                    <div class="card-body">
                        <div id="chart-3"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">Graphique 4</div>
                    <div class="card-body">
                        <div id="chart-4"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
