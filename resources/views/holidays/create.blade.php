@extends('layouts.app')

@section('content')
    <main role="main">

        <div class="container">
            <h1>Nouvelles vacances publiques / Jour férié</h1>
            <hr>
            <form method="post" action="{{ route('holidays.store') }}">
                <div class="form-group">
                    <label for="name">Nom</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Nom du jour férié" required>
                    <small class="form-text text-muted">Ex. : Noël, Vendredi Saint, Ascension, etc...</small>
                </div>
                <div class="form-group">
                    <label for="date">Date</label>
                    <input type="text" class="form-control datepicker" name="date" id="date" placeholder="Date du jour férié">
                </div>
                {{ csrf_field() }}
                <button type="submit" class="btn btn-primary">Ajouter</button>
                <a href="{{ route('config') }}" class="btn btn-secondary">Annuler</a>
            </form>
            @if ($errors->any())
                <div class="alert alert-danger mt-3" role="alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>

    </main>

@endsection
