<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'BT Plan') }} - Gestion des absences</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">

    <!-- Select2 -->
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/select2-bootstrap.min.css') }}" rel="stylesheet" />

    <link href="{{ asset('css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('css/colpick.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('css/fullcalendar.min.css') }}" >
    <link rel="stylesheet" href="{{ asset('css/app.css') }}?3" >

    <link rel="stylesheet" href="{{ asset('css/frappe-charts.min.css') }}">
</head>

<body style="padding-top: 3.5rem">

    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark" role="navigation">
        <div class="container">
            <a class="navbar-brand" href="/">{{ config('app.name', 'Laravel') }}</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
                    aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('calendar') }}"><i class="fa fa-calendar" aria-hidden="true"></i> Calendrier</a>
                    </li>
                    <!--<li class="nav-item">
                        <a class="nav-link" href="{{ route('stats.index') }}"><i class="fa fa-bar-chart" aria-hidden="true"></i> Stats</a>
                    </li>-->
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('users.index') }}"><i class="fa fa-users" aria-hidden="true"></i> Utilisateurs</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('config') }}"><i class="fa fa-cogs" aria-hidden="true"></i> Config</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>