@extends('layouts.home')

@section('content')

    <main role="main">

        <!-- Main jumbotron for a primary marketing message or call to action -->
        <div class="jumbotron">
            <div class="container text-center">
                <h1 class="display-1">{{ config('app.name', 'Laravel') }}</h1>
                <p class="lead">Système de gestion des absences de l'IT</p>
                <p><a class="btn btn-primary btn-lg" href="/calendar" role="button"><i class="fa fa-calendar" aria-hidden="true"></i> Voir le calendrier &raquo;</a></p>
            </div>
        </div>

    </main>

@endsection