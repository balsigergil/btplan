@extends('layouts.app')

@section('content')
    <main role="main">

        <div class="container">
            <h1>Nouvel utilisateur</h1>
            <hr>
            <form method="post" action="{{ route('users.store') }}">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="firstname">Prénom</label>
                        <input type="text" class="form-control" name="firstname" id="firstname" placeholder="Prénom du l'utilisateur" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="lastname">Nom</label>
                        <input type="text" class="form-control" name="lastname" id="lastname" placeholder="Nom de l'utilisateur" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="muid">MUID / XUID</label>
                        <input type="text" class="form-control" name="muid" id="muid" placeholder="M/X de l'utilisateur" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="email">E-mail</label>
                        <input type="email" class="form-control" name="email" id="email" placeholder="E-mail de l'utilisateur" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="address">Adresse</label>
                    <input type="text" class="form-control" name="address" id="address" placeholder="Adresse de l'utilisateur">
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="birth">Anniversaire</label>
                        <input type="text" class="form-control datepicker" name="birth" id="birth">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="phone">N° de téléphone</label>
                        <div class="input-group">
                            <span class="input-group-addon">+41</span>
                            <input type="tel" class="form-control" id="phone" name="phone">
                        </div>
                    </div>
                </div>
                {{ csrf_field() }}
                <button type="submit" class="btn btn-primary">Ajouter</button>
                <a href="{{ route('users.index') }}" class="btn btn-secondary">Annuler</a>
            </form>
            @if ($errors->any())
                <div class="alert alert-danger mt-3" role="alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>

    </main>
@endsection
