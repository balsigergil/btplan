@extends('layouts.app')

@section('content')
    <main role="main">

        <div class="container">
            <h1>Configuration</h1>
            <hr>
            @if (session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('status') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            <h2 style="display: inline-block">Vacances publiques</h2>
            <a href="{{ route('holidays.create') }}" class="btn btn-success"
               style="display: inline-block; vertical-align: top"><i class="fa fa-plus"></i></a>

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                @foreach($years as $year)
					<?php $year == date( 'Y' ) ? $attr = 'active' : $attr = '' ?>
                    <li class="nav-item">
                        <a class="nav-link {{ $attr }}" id="{{ $year }}-tab" data-toggle="tab" href="#{{ $year }}"
                           role="tab"
                           aria-controls="{{ $year }}">{{ $year }}</a>
                    </li>
                @endforeach
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                @foreach($years as $year)
					<?php $year == date( 'Y' ) ? $attr = 'active' : $attr = '' ?>
                    <div class="tab-pane {{ $attr }}" id="{{ $year }}" role="tabpanel"
                         aria-labelledby="{{ $year }}-tab">
                        <table class="table">
                            <tr>
                                <th>Nom</th>
                                <th>Date</th>
                                <th>Actions</th>
                            </tr>
                            @foreach ($holidays as $holiday)
                                @if($holiday->date->year == $year)
                                    <tr>
                                        <td>{{ $holiday->name }}</td>
                                        <td>{{ $holiday->date->format('d.m.Y') }}</td>
                                        <td>
                                            <a href="{{ route('holidays.edit', $holiday->id) }}"
                                               class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                                            <a href="{{ route('holidays.destroy', $holiday->id) }}"
                                               class="btn btn-danger"
                                               onclick="return confirm('Êtes-vous sûr(e) de vouloir supprimer ce jour ?')"><i
                                                        class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </table>
                    </div>
                @endforeach
            </div>

            <h2 style="display: inline-block">Type d'absence</h2>
            <a href="{{ route('types.create') }}" class="btn btn-success" style="display: inline-block; vertical-align: top"><i class="fa fa-plus"></i></a>
            <table class="table">
                <tr>
                    <th>Nom</th>
                    <th>Couleur</th>
                    <th>Actions</th>
                </tr>
                @foreach ($types as $type)
                    <tr>
                        <td>{{ $type->name }}</td>
                        <td>
                            <span style="background-color:{{ $type->color }};color: white; padding: 5px;border-radius: .25rem;">
                                {{ $type->color }}
                            </span>
                        </td>
                        <td>
                            <a href="{{ route('types.edit', $type->id) }}"
                               class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                            <a href="{{ route('types.destroy', $type->id) }}"
                               class="btn btn-danger"
                               onclick="return confirm('Êtes-vous sûr(e) de vouloir supprimer ce type d\'absence ?')"><i
                                        class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                @endforeach
            </table>

        </div>

    </main>
@endsection
