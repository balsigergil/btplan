@extends('layouts.app')

@section('content')
    <main role="main">

        <div class="container">
            <h1>Modification de {{ $type->name }}</h1>
            <hr>
            <form method="post" action="{{ route('types.update', $type->id) }}">
                <div class="form-group">
                    <label for="name">Nom</label>
                    <input type="text" class="form-control" name="name" id="name" value="{{ $type->name }}" placeholder="Nom du type d'absence">
                    <small class="form-text text-muted">Par ex. : Vacances, maladie, piquet, etc...</small>
                </div>
                <div class="form-group">
                    <label for="color">Couleur</label>
                    <input type="text" class="form-control" name="color" id="color" value="{{ $type->color }}">
                </div>
                {{ csrf_field() }}
                <button type="submit" class="btn btn-primary">Modifier</button>
                <a href="{{ route('config') }}" class="btn btn-secondary">Annuler</a>
            </form>
            @if ($errors->any())
                <div class="alert alert-danger mt-3" role="alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>

    </main>
@endsection
