<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Resources\LeaveResource;
use App\Leave;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

// Holidays
Route::get('/holidays/create', 'HolidayController@create')->name('holidays.create');
Route::post('/holidays', 'HolidayController@store')->name('holidays.store');
Route::get('/holidays/{holiday}/delete', 'HolidayController@destroy')->name('holidays.destroy');
Route::get('/holidays/{holiday}/edit', 'HolidayController@edit')->name('holidays.edit');
Route::post('/holidays/{holiday}', 'HolidayController@update')->name('holidays.update');

// Leave types
Route::get('/types/create', 'TypeController@create')->name('types.create');
Route::post('/types', 'TypeController@store')->name('types.store');
Route::get('/types/{type}/delete', 'TypeController@destroy')->name('types.destroy');
Route::get('/types/{type}/edit', 'TypeController@edit')->name('types.edit');
Route::post('/types/{type}', 'TypeController@update')->name('types.update');

// Users
Route::resource('users', 'UserController');
Route::get('/births', 'UserController@getAllBirths')->name('births');

// Leaves
Route::get('/calendar', 'LeaveController@index')->name('calendar');
Route::post('/leaves', 'LeaveController@store')->name('leaves.store');
Route::post('/leaves/{leave}', 'LeaveController@update')->name('leaves.update');
Route::get('/leaves/{leave}/delete', 'LeaveController@delete')->name('leaves.delete');
Route::get('/leaves', 'LeaveController@getAll')->name('leaves');

Route::get('/config', 'ConfigController@index')->name('config');

// Stats
Route::get('/statistics', 'StatsController@index')->name('stats.index');

Auth::routes();
