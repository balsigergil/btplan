namespace :php do

    desc 'Composer PHP dependencies installation'
    task :composer do

        on roles(:web) do
            within release_path do
                execute :composer, :install
            end
        end

    end

end